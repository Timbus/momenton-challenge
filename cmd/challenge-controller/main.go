package main

import (
	"flag"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"

	annotation_checker "bitbucket.com/Timbus/momenton-challenge/internal"
)

const magicAnnotation = "challenge.momenton.com.au/momenton"

func main() {
	sendEmail := flag.Bool("email-alerts", true, "Set if you want to send alerts via email")
	smtpHost := flag.String("smtp-host", "localhost", "The SMTP server URL")
	smtpPort := flag.Int("smtp-port", 25, "The SMTP server port")
	smtpUser := flag.String("smtp-user", "user", "The SMTP server username")
	smtpPass := flag.String("smtp-pass", "password", "The SMTP server password")
	alertRecipient := flag.String("email-recipient", "devops@company.com", "The poor guy who has to deal with this")

	flag.Parse()

	// Fetch kube config from container
	config, err := rest.InClusterConfig()
	if err != nil {
		panic(err.Error())
	}

	clientSet, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}

	var emailSettings *annotation_checker.EmailSettings
	if *sendEmail {
		emailSettings = &annotation_checker.EmailSettings{
			AlertRecipient: *alertRecipient,
			SmtpHost:       *smtpHost,
			SmtpPort:       *smtpPort,
			SmtpUser:       *smtpUser,
			SmtpPass:       *smtpPass,
		}
	}

	ac := annotation_checker.AnnotationChecker{
		TargetAnnotation: magicAnnotation,
		KubernetesClient: clientSet,
		EmailSettings:    emailSettings,
	}

	ac.LoopForever()

}
