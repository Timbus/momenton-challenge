FROM golang:1.12-alpine3.9 as builder
RUN apk add --no-cache git

WORKDIR /app

# Cache go modules so subsequent builds are much faster
COPY go.mod go.sum ./
RUN go mod download

# Build standalone app
ADD . /app
RUN go build -o challenge-controller ./cmd/challenge-controller/main.go

# Throw app in bare container
FROM alpine:3.9 as runtime

COPY --from=builder /app/challenge-controller /bin/challenge-controller

CMD /bin/challenge-controller
