package annotation_checker

import (
	"fmt"
	"time"

	"gopkg.in/gomail.v2"
	policy "k8s.io/api/policy/v1beta1"
	metaopts "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

type EmailSettings struct {
	AlertRecipient string
	SmtpHost       string
	SmtpPort       int
	SmtpUser       string
	SmtpPass       string
}

type AnnotationChecker struct {
	// The annotation we are checking for:
	TargetAnnotation string
	// The Kube client:
	KubernetesClient *kubernetes.Clientset

	// FIXME: Violates SRP, refactor:
	EmailSettings *EmailSettings
}

func (ac AnnotationChecker) LoopForever() {
	for {
		podList, err := ac.KubernetesClient.CoreV1().Pods("").List(metaopts.ListOptions{})
		if err != nil {
			panic(err.Error())
		}

		for _, pod := range podList.Items {
			annotations := pod.GetAnnotations()
			if val, ok := annotations[ac.TargetAnnotation]; ok && val == "true" {
				fmt.Printf("Pod %s has the annotation! Yay!\n", pod.GetName())
			} else {
				fmt.Printf("Oh no, no annotation for pod %s!\n", pod.GetName())

				// Could alert Slack using "nlopes/slack":
				// id, timestamp, err := slackAPI.PostMessage("CHANNEL_ID", slack.MsgOptionText("Oh no! Pod %s is missing the company-mandated annotation!", true))

				// Send email, if SMTP settings are given:
				if ac.EmailSettings != nil {
					msg := gomail.NewMessage()
					msg.SetHeader("From", "controller@site.com")
					msg.SetHeader("To", ac.EmailSettings.AlertRecipient)
					msg.SetHeader("Subject", "Oh no! Pod misconfigured")
					msg.SetBody("text/plain", fmt.Sprintf(`Your pod "%s" in namespace "%s" is missing a magic annotation! Plz fix`, pod.GetName(), pod.GetNamespace()))

					mailer := gomail.NewDialer(ac.EmailSettings.SmtpHost, ac.EmailSettings.SmtpPort, ac.EmailSettings.SmtpUser, ac.EmailSettings.SmtpPass)
					mailer.DialAndSend(msg)
				}

				// Gracefully shut down pod:
				fmt.Println("Shutting it down...")
				ac.KubernetesClient.PolicyV1beta1().Evictions(pod.GetNamespace()).Evict(
					&policy.Eviction{
						TypeMeta: metaopts.TypeMeta{Kind: "Eviction"},
						ObjectMeta: metaopts.ObjectMeta{
							Name:      pod.GetName(),
							Namespace: pod.GetNamespace(),
						},
					},
				)

				// The criteria says to prevent the pod being scheduled. But that would need me to edit the deployment, which I can only guess from the labels.. Hm
			}
		}
		time.Sleep(15 * time.Second)
	}
}
